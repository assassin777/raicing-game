﻿using Script.Controllers.Cameras;
using Script.Controllers.Game;
using Script.Controllers.Levels;
using UnityEngine;

namespace Script.Panels.Manegers
{
    public class StartGame : MonoBehaviour
    {
        [SerializeField] private LevelController[] _levels;
        [SerializeField] private GameObject _car;
        [SerializeField] private GameObject _canvas;
        [SerializeField] private CameraController _cameraController;

        private void Start()
        {
            var uiManager = _canvas.AddComponent<UIManager>();
            uiManager.Init();
            var gameController = gameObject.AddComponent<GameController>();
            gameController.init(uiManager, _car, _levels, _cameraController);
        }
    }
}