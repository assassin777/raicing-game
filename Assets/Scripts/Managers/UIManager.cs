﻿using Scripts;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Script.Panels.Manegers
{
    public class UIManager : MonoBehaviour
    {
        private PanelBase[] _panels;
        private PanelBase _currentPanel;

        public void Init()
        {
            InitPages();
        }

        private void InitPages()
        {
            var panels = new List<PanelBase>();
            foreach (Transform child in transform)
            {
                var panel = child.GetComponent<PanelBase>();
                if (panel == null)
                {
                    continue;
                }

                panel.Init(this);
                panel.gameObject.SetActive(false);
                panels.Add(panel);
            }

            _panels = panels.ToArray();
        }

        public void Open(PanelId panelId, PanelArgs panelArgs)
        {
            if (_currentPanel != null)
            {
                _currentPanel.Hide(OnHidden);
            }
            _currentPanel = Switch(panelId, panelArgs);
        }

        private PanelBase Switch(PanelId panelId, PanelArgs panelArgs)
        {
            var panel = _panels.First(x => x.panelId.Equals(panelId));
            panel.Display(panelArgs, OnDisplayed);
            return panel;
        }

        private void OnDisplayed(PanelId panelId)
        {
            Debug.Log($"Display {panelId}.");
        }
        private void OnHidden(PanelId panelId)
        {
            Debug.Log($"Hide {panelId}.");
        }
    }
}
