﻿using Scripts;
using UnityEngine;

public abstract class PanelArgs : MonoBehaviour
{
    public abstract PanelId panelId { get; }
}
