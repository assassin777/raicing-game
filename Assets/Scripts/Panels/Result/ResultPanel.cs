﻿using Script.Panels.Manegers;
using Scripts;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Script.Panels.Result
{
    public class ResultPanel : PanelBaseGeneric<ResultPanelArgs>
    {
        public override PanelId panelId => PanelId.Result;

        [SerializeField] private Button _restart;
        [SerializeField] private Button _continue;
        [SerializeField] private GameObject[] _stars;

        protected override void OnInit(UIManager manager)
        {
            _restart.onClick.AddListener(OnRestartClick);
            _continue.onClick.AddListener(OnContinueClick);
        }

        protected override void OnDisplayStart(PanelArgs args, Action<PanelId> onDisplayed)
        {
            base.OnDisplayStart(args, onDisplayed);

            _continue.gameObject.SetActive(PanelArgs.CanContinue);

            if (PanelArgs.RaceResultInfo.LevelStar == LevelStars.NoStar)
            {
                Array.ForEach(_stars, star => star.SetActive(false));
            }
            else
            {
                for (int i = 0; i <= (int)PanelArgs.RaceResultInfo.LevelStar; i++)
                {
                    _stars[i].SetActive(true);
                }
                for (int i = (int)(PanelArgs.RaceResultInfo.LevelStar + 1); i <= (int)LevelStars.Star3; i++)
                {
                    _stars[i].SetActive(false);
                }
            }
        }

        private void OnContinueClick()
        {
            PanelArgs.ContinueRace();
        }

        private void OnRestartClick()
        {
            PanelArgs.RestartRace();
        }
    }
}
