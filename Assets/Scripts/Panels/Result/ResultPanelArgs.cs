﻿using Script.Controllers.Game;
using Scripts;
using System;

namespace Script.Panels.Result
{
    public class ResultPanelArgs : PanelArgs
    {
        public override PanelId panelId => PanelId.Result;

        public readonly RaceResultInfo RaceResultInfo;

        public bool CanContinue;

        public readonly Action RestartRace;
        public readonly Action ContinueRace;

        public ResultPanelArgs(RaceResultInfo raceResultInfo, bool canContinue, Action restartRace, Action continueRace)
        {
            RaceResultInfo = raceResultInfo;
            CanContinue = canContinue;
            RestartRace = restartRace;
            ContinueRace = continueRace;
        }
    }
}
