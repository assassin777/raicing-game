﻿using Scripts;
using System;

public abstract class PanelBaseGeneric<T> : PanelBase where T : PanelArgs
{
    protected T PanelArgs { get; private set; }

    protected override void OnDisplayStart(PanelArgs args, Action<PanelId> onDisplayed)
    {
        PanelArgs = args as T;
        gameObject.SetActive(true);
        IsDisplayed = true;
        onDisplayed?.Invoke(panelId);
    }

    protected override void OnHidingBegin(Action<PanelId> onComplete)
    {
        gameObject.SetActive(false);
        IsDisplayed = false;
        onComplete?.Invoke(panelId);
    }
}
