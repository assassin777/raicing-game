﻿using Script.Panels.Manegers;
using Script.Panels.Menu;
using Scripts;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Script.Panels.Race
{
    public class RacePanel : PanelBaseGeneric<RacePanelArgs>
    {
        public override PanelId panelId => PanelId.Race;

        [SerializeField] Button Menu;
        [SerializeField] Text _checkPoints;
        [SerializeField] Text _lastPointTime;
        [SerializeField] Text _timeSpan;
        [SerializeField] Text _Distance;
        [SerializeField] Text _totalMeters;
        [SerializeField] Text _speed;

        private MenuPanelArgs _menuPanelArgs;

        protected override void OnInit(UIManager manager)
        {
            Menu.onClick.AddListener(OnMenuClick);
        }

        protected override void OnDisplayStart(PanelArgs args, Action<PanelId> onDisplayed)
        {
            base.OnDisplayStart(args, onDisplayed);
            _menuPanelArgs = new MenuPanelArgs(PanelArgs);
        }

        private void OnMenuClick()
        {
            UiManager.Open(_menuPanelArgs.panelId, _menuPanelArgs);
        }

        private void Update()
        {
            _checkPoints.text = $"checkPoints {PanelArgs.LevelInfo.CurrentPointIndex}/{PanelArgs.LevelInfo.PointsNumber}";
            _lastPointTime.text = PanelArgs.LevelInfo.LastPointAchivedTime == null
                ? "No checkPoints achived"
                : $"CheckPoint time {PanelArgs.LevelInfo.LastPointAchivedTime:0.00}";
            _timeSpan.text = $"Race time {PanelArgs.LevelInfo.RaceTimeSpan:0.00}";
            _Distance.text = $"Race Length {PanelArgs.LevelInfo.RaceLength}";
            _totalMeters.text = $"Race meters {PanelArgs.LevelInfo.PassedMeters:0.00}";
            _speed.text = $"{(Mathf.Abs(PanelArgs.LevelInfo.Speed) * 3.6):0.00} km/h";
        }
    }
}