﻿
using Script.Controllers.Levels;
using Scripts;

namespace Script.Panels.Race
{
    public class RacePanelArgs : PanelArgs
    {
        public override PanelId panelId => PanelId.Race;

        public readonly LevelInfo LevelInfo;

        public RacePanelArgs(LevelInfo levelInfo)
        {
            LevelInfo = levelInfo;
        }
    }
}