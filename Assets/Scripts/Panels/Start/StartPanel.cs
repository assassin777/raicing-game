﻿using Script.Panels.Manegers;
using Scripts;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Script.Panels.Start
{
    public class StartPanel : PanelBaseGeneric<StartPanelArgs>
    {
        public override PanelId panelId => PanelId.Start;

        [SerializeField] private Button _exit;
        [SerializeField] private Button _start;
        [SerializeField] private Button _continue;

        protected override void OnInit(UIManager manager)
        {
            _exit.onClick.AddListener(OnExitClick);
            _start.onClick.AddListener(OnStartClick);
            _continue.onClick.AddListener(OnContinueClick);
        }

        protected override void OnDisplayStart(PanelArgs args, Action<PanelId> onDisplayed)
        {
            base.OnDisplayStart(args, onDisplayed);

            _continue.gameObject.SetActive(PanelArgs.GameInfo.AvailableLevel > 0);
        }

        private void OnContinueClick()
        {
            PanelArgs.OnContinue();
        }

        private void OnStartClick()
        {
            PanelArgs.OnStart();
        }

        private void OnExitClick()
        {
            Application.Quit();
        }
    }

}