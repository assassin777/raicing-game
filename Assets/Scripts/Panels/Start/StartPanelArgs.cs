﻿using Script.Controllers.Game;
using Scripts;
using System;

namespace Script.Panels.Start
{
    public class StartPanelArgs : PanelArgs
    {
        public override PanelId panelId => PanelId.Start;

        public readonly GameInfo GameInfo;
        public readonly Action OnStart;
        public readonly Action OnContinue;

        public StartPanelArgs(GameInfo gameInfo, Action onStart, Action onContinue)
        {
            GameInfo = gameInfo;
            OnStart = onStart;
            OnContinue = onContinue;
        }
    }
}
