﻿using Script.Panels.Manegers;
using Scripts;
using System;
using UnityEngine;

public abstract class PanelBase : MonoBehaviour
{
    public abstract PanelId panelId { get; }
    public bool IsDisplayed { get; set; }
    protected UIManager UiManager;

    public void Init(UIManager manager)
    {
        UiManager = manager;
        OnInit(manager);
    }

    public void Display(PanelArgs pageArgs, Action<PanelId> onDisplayed)
    {
        OnDisplayStart(pageArgs, onDisplayed);
    }

    public void Hide(Action<PanelId> onHidden)
    {
        OnHidingBegin(onHidden);
    }

    protected abstract void OnInit(UIManager manager);
    protected abstract void OnDisplayStart(PanelArgs pageArgs, Action<PanelId> onDisplayed);
    protected abstract void OnHidingBegin(Action<PanelId> onComplete);
}
