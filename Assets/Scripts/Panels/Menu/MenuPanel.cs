﻿using Script.Controllers.Game;
using Script.Panels.Manegers;
using Scripts;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Script.Panels.Menu
{
    public class MenuPanel : PanelBaseGeneric<MenuPanelArgs>
    {
        public override PanelId panelId => PanelId.Menu;

        [SerializeField] Button _resume;
        [SerializeField] Button _restart;
        [SerializeField] Button _startPanel;

        protected override void OnInit(UIManager manager)
        {
            _resume.onClick.AddListener(OnResume);
            _restart.onClick.AddListener(OnRestart);
            _startPanel.onClick.AddListener(OnStartPanel);
        }

        protected override void OnDisplayStart(PanelArgs args, Action<PanelId> onDisplayed)
        {
            base.OnDisplayStart(args, onDisplayed);

            GameController.SetPause(true);
        }

        protected override void OnHidingBegin(Action<PanelId> onComplete)
        {
            base.OnHidingBegin(onComplete);

            GameController.SetPause(false);
        }

        private void OnStartPanel()
        {
            PanelArgs.GameController.ExitFromRace();
        }

        private void OnRestart()
        {
            PanelArgs.GameController.RestartRace();
        }

        private void OnResume()
        {
            UiManager.Open(PanelArgs.RacePanelArgs.panelId, PanelArgs.RacePanelArgs);
        }
    }
}
