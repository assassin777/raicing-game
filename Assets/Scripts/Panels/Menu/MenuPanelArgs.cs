﻿
using Script.Controllers.Game;
using Script.Panels.Race;
using Scripts;

namespace Script.Panels.Menu
{
    public class MenuPanelArgs : PanelArgs
    {
        public override PanelId panelId => PanelId.Menu;

        public readonly RacePanelArgs RacePanelArgs;
        public readonly GameController GameController;

        public MenuPanelArgs(RacePanelArgs panelArgs)
        {
            RacePanelArgs = panelArgs;
            GameController = FindObjectOfType<GameController>();
        }
    }
}
