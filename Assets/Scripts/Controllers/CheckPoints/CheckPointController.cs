﻿using Script.Controllers.Cars;
using System;
using UnityEngine;

namespace Script.Controllers.CheckPoints
{
    [RequireComponent(typeof(Collider))]
    public class CheckPointController : MonoBehaviour
    {
        public event Action OnAchiveCheckPoint = delegate { };

        private bool _isTarget;

        public void Init()
        {
            gameObject.SetActive(false);
        }

        public void Play(bool play)
        {
            gameObject.SetActive(play);
            _isTarget = play;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!_isTarget)
                return;

            if (other.GetComponentInParent<CarController>() == null && other.GetComponent<CarController>() == null)
            {
                return;
            }

            OnAchiveCheckPoint();
            Play(false);
        }
    }
}
