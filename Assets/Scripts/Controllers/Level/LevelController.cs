﻿using Script.Controllers.Cameras;
using Script.Controllers.Cars;
using Script.Controllers.CheckPoints;
using Script.Controllers.Game;
using Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Script.Controllers.Levels
{
    public class LevelController : MonoBehaviour
    {
        [System.Serializable]
        public class LevelResultInfo
        {
            public float secondsForStar1;
            public float secondsForStar2;
            public float secondsForStar3;
        }

        [SerializeField] private float _traceLength;

        [SerializeField] private GameObject _startPoint;
        [SerializeField] private CheckPointController[] _checkPoints;
        [SerializeField] private LevelResultInfo _levelResultInfo;

        private LevelInfo _levelInfo;
        private GameObject _car;
        private CheckPointController _next;
        private CameraController _cameraController;

        private ISpeedCalculator _speedCalculator;

        private int _currentCheckPoint;
        private float _startTime;
        private float? _lastPointStart = null;
        private float meters;
        private bool _play;

        public event Action<RaceResultInfo> OnFinish = delegate { };

        public LevelInfo Play(GameObject car, CameraController cameraController)
        {
            Array.ForEach(_checkPoints, checkPoint => checkPoint.Init());

            _cameraController = cameraController;
            _car = Instantiate(car, _startPoint.transform);
            _speedCalculator = _car.GetComponent<ISpeedCalculator>();
            _cameraController.Play(_car.gameObject, _speedCalculator);
            _levelInfo = new LevelInfo(_checkPoints.Length, _traceLength);

            InitPlay();

            _play = true;

            return _levelInfo;
        }

        public void InitPlay()
        {
            meters = 0;
            _startTime = Time.time;
            _currentCheckPoint = 0;
            PlayNextCheckPoint();
        }

        public void Restart()
        {
            if (_next != null)
            {
                _next.Play(false);
                _lastPointStart = null;
            }

            _cameraController.UpdatePosition();
            _car.GetComponent<CarController>().Restart();

            InitPlay();
        }

        public void Exit()
        {
            _play = false;
            _cameraController.Stop();
            Destroy(gameObject);
        }

        private void Update()
        {
            if (!_play)
            {
                return;
            }

            float timeSpan = Time.time - _startTime;
            float? lastPointAchivedTime = Time.time - _lastPointStart;
            float speed = _speedCalculator.SpeedMetersPerSecond();
            meters += (speed * Time.deltaTime);

            _levelInfo.UpdateInfo(_currentCheckPoint, lastPointAchivedTime, timeSpan, meters, speed);
        }

        private void PlayNextCheckPoint()
        {
            _next = _checkPoints[_currentCheckPoint];
            _next.Play(true);
            _next.OnAchiveCheckPoint += OnAchiveCheckPoint;
        }

        private void OnAchiveCheckPoint()
        {
            _next.OnAchiveCheckPoint -= OnAchiveCheckPoint;
            _lastPointStart = Time.time;

            _currentCheckPoint++;

            if (_currentCheckPoint < _checkPoints.Length)
            {
                PlayNextCheckPoint();
            }
            else
            {
                float timeSpan = Time.time - _startTime;
                var levelStar = CalculateStart(timeSpan);
                var resultInfo = new RaceResultInfo(levelStar, timeSpan);
                OnFinish(resultInfo);
            }
        }

        private LevelStars CalculateStart(float timeSpan)
        {
            if (timeSpan <= _levelResultInfo.secondsForStar1)
            {
                return LevelStars.Star1;
            }
            else
            if (timeSpan <= _levelResultInfo.secondsForStar2)
            {
                return LevelStars.Star2;
            }
            else
            if (timeSpan <= _levelResultInfo.secondsForStar3)
            {
                return LevelStars.Star3;
            }
            else
            {
                return LevelStars.NoStar;
            }
        }
    }
}
