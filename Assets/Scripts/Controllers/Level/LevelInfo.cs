﻿using UnityEngine;

namespace Script.Controllers.Levels
{
    public class LevelInfo : MonoBehaviour
    {
        public int CurrentPointIndex { get; private set; }
        public float? LastPointAchivedTime { get; private set; }
        public float RaceTimeSpan { get; private set; }
        public float PassedMeters { get; private set; }
        public float Speed { get; private set; }

        public readonly int PointsNumber;
        public readonly float RaceLength;

        public LevelInfo(int pointsNumber, float raceLength)
        {
            PointsNumber = pointsNumber;
            RaceLength = raceLength;
        }

        public void UpdateInfo(int currentPointIndex, float? lastPointAchivedTime, float raceTimeSpan, float passedMeters, float speed)
        {
            CurrentPointIndex = currentPointIndex;
            LastPointAchivedTime = lastPointAchivedTime;
            RaceTimeSpan = raceTimeSpan;
            PassedMeters = passedMeters;
            Speed = speed;
        }
    }
}

