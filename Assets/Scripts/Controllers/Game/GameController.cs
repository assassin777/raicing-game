﻿using Script.Controllers.Cameras;
using Script.Controllers.Levels;
using Script.Panels.Manegers;
using Script.Panels.Race;
using Script.Panels.Result;
using Script.Panels.Start;
using Scripts;
using UnityEngine;

namespace Script.Controllers.Game
{
    public class GameController : MonoBehaviour
    {
        public static bool pauseMode { get; private set; }

        private GameInfo _gameInfo;
        private UIManager _uiManager;
        private GameObject _car;
        private LevelController[] _levels;
        private LevelController _currentLevel;
        private StartPanelArgs _startPanelArgs;
        private RacePanelArgs _racePanelArgs;
        private CameraController _cameraController;

        private int _currentLevelIndex;

        public void init(UIManager uiManager, GameObject car, LevelController[] levels, CameraController cameraController)
        {
            pauseMode = false;

            _car = car;
            _cameraController = cameraController;

            _uiManager = uiManager;
            _levels = levels;

            _gameInfo = new GameInfo();
            _startPanelArgs = new StartPanelArgs(_gameInfo, StartFirstLevel, Continue);
            uiManager.Open(_startPanelArgs.panelId, _startPanelArgs);
        }

        public static void SetPause(bool pause)
        {
            Time.timeScale = pause ? 0 : 1;
            pauseMode = pause;
        }

        public void ExitFromRace()
        {
            _currentLevel.Exit();
            _uiManager.Open(_startPanelArgs.panelId, _startPanelArgs);
        }

        public void RestartRace()
        {
            _currentLevel.Restart();
            _uiManager.Open(_racePanelArgs.panelId, _racePanelArgs);
        }

        private void Continue()
        {
            _currentLevelIndex = _gameInfo.AvailableLevel;
            StartRace(_currentLevelIndex);
        }

        private void StartFirstLevel()
        {
            _currentLevelIndex = 0;
            StartRace(_currentLevelIndex);
        }

        private void StartRace(int levelIndex)
        {
            _currentLevel = Instantiate(_levels[levelIndex].gameObject, transform).GetComponent<LevelController>();
            LevelInfo levelInfo = _currentLevel.Play(_car, _cameraController);
            _racePanelArgs = new RacePanelArgs(levelInfo);
            _uiManager.Open(_racePanelArgs.panelId, _racePanelArgs);

            _currentLevel.OnFinish += OnFinishRace;
        }

        private void OnFinishRace(RaceResultInfo result)
        {
            CalcAvailableLevel(result.LevelStar);
            var resultArgs = new ResultPanelArgs(result, CanContinue(), RestartRace, Continue);
            _uiManager.Open(resultArgs.panelId, resultArgs);
        }

        private void CalcAvailableLevel(LevelStars levelStar)
        {
            if (levelStar < LevelStars.NoStar)
            {
                int nextLevel = _currentLevelIndex + 1;
                _gameInfo.SetAvailable(nextLevel);
            }
        }

        private bool CanContinue()
        {
            return _gameInfo.AvailableLevel > _currentLevelIndex;
        }
    }
}
