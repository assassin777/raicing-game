﻿
namespace Script.Controllers.Game
{
    public class GameInfo
    {
        public int AvailableLevel { get; private set; }
        public int CurretLevel { get; private set; }

        public GameInfo()
        {
            AvailableLevel = 0;
            CurretLevel = 0;
        }

        public void SetAvailable(int nextLevel)
        {
            AvailableLevel = nextLevel;
        }
    }
}