﻿using Scripts;
using UnityEngine;

namespace Script.Controllers.Game
{
    public class RaceResultInfo : MonoBehaviour
    {
        public readonly LevelStars LevelStar;
        public readonly float RaceTime;

        public RaceResultInfo(LevelStars levelStar, float timeSpan)
        {
            LevelStar = levelStar;
            RaceTime = timeSpan;
        }
    }
}
