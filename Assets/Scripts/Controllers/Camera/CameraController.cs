﻿using Script.Controllers.Cars;
using Script.Controllers.Game;
using UnityEngine;

namespace Script.Controllers.Cameras
{
    [RequireComponent(typeof(Camera))]
    [RequireComponent(typeof(AngleRotateAroundInput))]
    public class CameraController : MonoBehaviour
    {
        [SerializeField]
        private AnimationCurve speedCurve;

        private Camera _mainCamera;
        private GameObject _target;
        private AngleRotateAroundInput angleViewInput;
        private ISpeedCalculator _iSpeedCalculator;

        private float _angleRotateAround;
        private float _distance;
        private float _cameraHeightOffset;
        private float _targetHeightOffset;

        private Vector3 _lastFramePosition;
        private Vector3 _originPosition;

        private bool _isPlaying;

        public void Stop()
        {
            _isPlaying = false;
        }

        public void Play(GameObject gameObject, ISpeedCalculator speedCalculator)
        {
            _target = gameObject;
            _iSpeedCalculator = speedCalculator;
            _isPlaying = true;

            UpdatePosition();
        }

        public void UpdatePosition()
        {
            _lastFramePosition = _originPosition;
        }

        private void Awake()
        {
            _mainCamera = GetComponent<Camera>();
            angleViewInput = GetComponent<AngleRotateAroundInput>();

            _angleRotateAround = 0.0f;
            _distance = 5;
            _cameraHeightOffset = 2;
            _targetHeightOffset = 0;

            _originPosition = transform.position;
        }

        private void Update()
        {
            if (!_isPlaying || GameController.pauseMode)
            {
                return;
            }

            angleViewInput.UpdateAngle(ref _angleRotateAround);

            Vector3 cameraPos = _lastFramePosition;
            Vector3 targetPos = _target.transform.position;

            cameraPos.y = 0.0f;
            targetPos.y = 0.0f;

            Vector3 directionXZ = cameraPos - targetPos;
            directionXZ.Normalize();

            cameraPos = targetPos + directionXZ * _distance;
            cameraPos.y = _target.transform.position.y + _cameraHeightOffset;
            transform.position = cameraPos;


            targetPos = _target.transform.position;
            targetPos.y += _targetHeightOffset;

            Vector3 cameraForward = targetPos - cameraPos;
            Quaternion lookRotation = Quaternion.LookRotation(cameraForward, Vector3.up);
            transform.rotation = lookRotation;
            transform.RotateAround(targetPos, Vector3.up, _angleRotateAround);

            _lastFramePosition = cameraPos;

            if (_iSpeedCalculator != null)
            {
                float speed = _iSpeedCalculator.SpeedMetersPerSecond();
                float fieldOfView = speedCurve.Evaluate(speed);
                _mainCamera.fieldOfView = fieldOfView;
            }
        }
    }
}
