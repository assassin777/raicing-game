﻿using UnityEngine;

namespace Script.Controllers.Cameras
{
    public class AngleRotateAroundInput : MonoBehaviour
    {
        public void UpdateAngle(ref float angleView)
        {
            if (Input.GetKey(KeyCode.Alpha1) || Input.GetKey(KeyCode.F1))
            {
                angleView = 60.0f;
            }

            if (Input.GetKey(KeyCode.Alpha2) || Input.GetKey(KeyCode.F2))
            {
                angleView = -60.0f;
            }

            if (Input.GetKey(KeyCode.Alpha3) || Input.GetKey(KeyCode.F3))
            {
                angleView = 0.0f;
            }

            if (Input.GetKey(KeyCode.Alpha4) || Input.GetKey(KeyCode.F4))
            {
                angleView = 180.0f;
            }
        }
    }
}
