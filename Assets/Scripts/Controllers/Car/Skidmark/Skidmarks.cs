﻿using UnityEngine;
using UnityEngine.Rendering;

namespace Script.Controllers.Cars.SkidMark
{
    public class Skidmarks : MonoBehaviour
    {
        [SerializeField] private Material skidmarksMaterial;

        private const int MAX_MARKS = 2048;
        private const float MARK_WIDTH = 0.35f;
        private const float GROUND_OFFSET = 0.02f;
        private const float MIN_DISTANCE = 0.25f;
        private const float MIN_SQR_DISTANCE = MIN_DISTANCE * MIN_DISTANCE;

        private class MarkSection
        {
            public Vector3 Pos = Vector3.zero;
            public Vector3 Normal = Vector3.zero;
            public Vector4 Tangent = Vector4.zero;
            public Vector3 Posl = Vector3.zero;
            public Vector3 Posr = Vector3.zero;
            public byte Intensity;
            public int LastIndex;
        };

        private int _markIndex;
        private MarkSection[] _skidmarks;
        private Mesh _marksMesh;
        private MeshRenderer _meshRenderer;
        private MeshFilter _meshFilter;

        private Vector3[] _vertices;
        private Vector3[] _normals;
        private Vector4[] _tangents;
        private Color32[] _colors;
        private Vector2[] _uvs;
        private int[] _triangles;

        private bool _meshUpdated;
        private bool _haveSetBounds;

        private void Start()
        {
            _skidmarks = new MarkSection[MAX_MARKS];
            for (int i = 0; i < MAX_MARKS; i++)
            {
                _skidmarks[i] = new MarkSection();
            }

            _meshFilter = GetComponent<MeshFilter>();
            _meshRenderer = GetComponent<MeshRenderer>();
            if (_meshRenderer == null)
            {
                _meshRenderer = gameObject.AddComponent<MeshRenderer>();
            }
            _marksMesh = new Mesh();
            _marksMesh.MarkDynamic();
            if (_meshFilter == null)
            {
                _meshFilter = gameObject.AddComponent<MeshFilter>();
            }
            _meshFilter.sharedMesh = _marksMesh;

            _vertices = new Vector3[MAX_MARKS * 4];
            _normals = new Vector3[MAX_MARKS * 4];
            _tangents = new Vector4[MAX_MARKS * 4];
            _colors = new Color32[MAX_MARKS * 4];
            _uvs = new Vector2[MAX_MARKS * 4];
            _triangles = new int[MAX_MARKS * 6];

            _meshRenderer.shadowCastingMode = ShadowCastingMode.Off;
            _meshRenderer.receiveShadows = false;
            _meshRenderer.material = skidmarksMaterial;
            _meshRenderer.lightProbeUsage = LightProbeUsage.Off;
        }

        private void LateUpdate()
        {
            if (!_meshUpdated) return;
            _meshUpdated = false;

            _marksMesh.vertices = _vertices;
            _marksMesh.normals = _normals;
            _marksMesh.tangents = _tangents;
            _marksMesh.triangles = _triangles;
            _marksMesh.colors32 = _colors;
            _marksMesh.uv = _uvs;

            if (!_haveSetBounds)
            {
                _marksMesh.bounds = new Bounds(new Vector3(0, 0, 0), new Vector3(10000, 10000, 10000));
                _haveSetBounds = true;
            }

            _meshFilter.sharedMesh = _marksMesh;
        }

        public int AddSkidMark(Vector3 pos, Vector3 normal, float intensity, int lastIndex)
        {
            if (intensity > 1) intensity = 1.0f;
            else if (intensity < 0) return -1;

            if (lastIndex > 0)
            {
                float sqrDistance = (pos - _skidmarks[lastIndex].Pos).sqrMagnitude;
                if (sqrDistance < MIN_SQR_DISTANCE) return lastIndex;
            }

            MarkSection curSection = _skidmarks[_markIndex];

            curSection.Pos = pos + normal * GROUND_OFFSET;
            curSection.Normal = normal;
            curSection.Intensity = (byte)(intensity * 255f);
            curSection.LastIndex = lastIndex;

            if (lastIndex != -1)
            {
                MarkSection lastSection = _skidmarks[lastIndex];
                Vector3 dir = (curSection.Pos - lastSection.Pos);
                Vector3 xDir = Vector3.Cross(dir, normal).normalized;

                curSection.Posl = curSection.Pos + xDir * MARK_WIDTH * 0.5f;
                curSection.Posr = curSection.Pos - xDir * MARK_WIDTH * 0.5f;
                curSection.Tangent = new Vector4(xDir.x, xDir.y, xDir.z, 1);

                if (lastSection.LastIndex == -1)
                {
                    lastSection.Tangent = curSection.Tangent;
                    lastSection.Posl = curSection.Pos + xDir * MARK_WIDTH * 0.5f;
                    lastSection.Posr = curSection.Pos - xDir * MARK_WIDTH * 0.5f;
                }
            }

            UpdateSkidmarksMesh();

            int curIndex = _markIndex;
            _markIndex = ++_markIndex % MAX_MARKS;

            return curIndex;
        }

        private void UpdateSkidmarksMesh()
        {
            MarkSection curr = _skidmarks[_markIndex];

            if (curr.LastIndex == -1) return;

            MarkSection last = _skidmarks[curr.LastIndex];
            _vertices[_markIndex * 4 + 0] = last.Posl;
            _vertices[_markIndex * 4 + 1] = last.Posr;
            _vertices[_markIndex * 4 + 2] = curr.Posl;
            _vertices[_markIndex * 4 + 3] = curr.Posr;

            _normals[_markIndex * 4 + 0] = last.Normal;
            _normals[_markIndex * 4 + 1] = last.Normal;
            _normals[_markIndex * 4 + 2] = curr.Normal;
            _normals[_markIndex * 4 + 3] = curr.Normal;

            _tangents[_markIndex * 4 + 0] = last.Tangent;
            _tangents[_markIndex * 4 + 1] = last.Tangent;
            _tangents[_markIndex * 4 + 2] = curr.Tangent;
            _tangents[_markIndex * 4 + 3] = curr.Tangent;

            _colors[_markIndex * 4 + 0] = new Color32(0, 0, 0, last.Intensity);
            _colors[_markIndex * 4 + 1] = new Color32(0, 0, 0, last.Intensity);
            _colors[_markIndex * 4 + 2] = new Color32(0, 0, 0, curr.Intensity);
            _colors[_markIndex * 4 + 3] = new Color32(0, 0, 0, curr.Intensity);

            _uvs[_markIndex * 4 + 0] = new Vector2(0, 0);
            _uvs[_markIndex * 4 + 1] = new Vector2(1, 0);
            _uvs[_markIndex * 4 + 2] = new Vector2(0, 1);
            _uvs[_markIndex * 4 + 3] = new Vector2(1, 1);

            _triangles[_markIndex * 6 + 0] = _markIndex * 4 + 0;
            _triangles[_markIndex * 6 + 2] = _markIndex * 4 + 1;
            _triangles[_markIndex * 6 + 1] = _markIndex * 4 + 2;

            _triangles[_markIndex * 6 + 3] = _markIndex * 4 + 2;
            _triangles[_markIndex * 6 + 5] = _markIndex * 4 + 1;
            _triangles[_markIndex * 6 + 4] = _markIndex * 4 + 3;

            _meshUpdated = true;
        }
    }
}
