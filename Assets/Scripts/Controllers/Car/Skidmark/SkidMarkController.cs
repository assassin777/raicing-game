﻿using UnityEngine;

namespace Script.Controllers.Cars.SkidMark
{
    public class SkidMarkController : MonoBehaviour
    {
        [SerializeField] private Rigidbody _rigidBody;

        private Skidmarks skidmarks;

        private WheelCollider wheelCollider;
        private WheelHit wheelHitInfo;

        private const float SKID_FX_SPEED = 0.5f;
        private const float MAX_SKID_INTENSITY = 20.0f;
        private const float WHEEL_SLIP_MULTIPLIER = 10.0f;
        private int lastSkid = -1;
        private float lastFixedUpdateTime;

        private void Awake()
        {
            wheelCollider = GetComponent<WheelCollider>();
            lastFixedUpdateTime = Time.time;

            skidmarks = FindObjectOfType<Skidmarks>();
        }

        private void FixedUpdate()
        {
            lastFixedUpdateTime = Time.time;
        }

        private void LateUpdate()
        {
            if (wheelCollider.GetGroundHit(out wheelHitInfo))
            {
                Vector3 localVelocity = transform.InverseTransformDirection(_rigidBody.velocity);
                float skidTotal = Mathf.Abs(localVelocity.x);

                float wheelAngularVelocity = wheelCollider.radius * ((2 * Mathf.PI * wheelCollider.rpm) / 60);
                float carForwardVel = Vector3.Dot(_rigidBody.velocity, transform.forward);
                float wheelSpin = Mathf.Abs(carForwardVel - wheelAngularVelocity) * WHEEL_SLIP_MULTIPLIER;

                wheelSpin = Mathf.Max(0, wheelSpin * (10 - carForwardVel));

                skidTotal += wheelSpin;

                if (skidTotal >= SKID_FX_SPEED)
                {
                    float intensity = Mathf.Clamp01(skidTotal / MAX_SKID_INTENSITY);
                    Vector3 skidPoint = wheelHitInfo.point + (_rigidBody.velocity * (Time.time - lastFixedUpdateTime));
                    lastSkid = skidmarks.AddSkidMark(skidPoint, wheelHitInfo.normal, intensity, lastSkid);
                }
                else
                {
                    lastSkid = -1;
                }
            }
            else
            {
                lastSkid = -1;
            }
        }
    }
}