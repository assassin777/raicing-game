﻿
namespace Script.Controllers.Cars
{
    public interface ISpeedCalculator
    {
        float SpeedMetersPerSecond();
    }
}