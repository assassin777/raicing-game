﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AerodynamicResistance : MonoBehaviour
{
    [SerializeField] private Vector3 coefficients = new Vector3(3.0f, 4.0f, 0.5f);

    private Rigidbody _rigidBody;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Vector3 localVelo = transform.InverseTransformDirection(_rigidBody.velocity);
        Vector3 absLocalVelo = new Vector3(Mathf.Abs(localVelo.x), Mathf.Abs(localVelo.y), Mathf.Abs(localVelo.z));
        Vector3 airResistance = Vector3.Scale(Vector3.Scale(localVelo, absLocalVelo), -2 * coefficients);
        _rigidBody.AddForce(transform.TransformDirection(airResistance));
    }
}
