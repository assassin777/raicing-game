﻿using Script.Controllers.Game;
using System.Collections.Generic;
using UnityEngine;

namespace Script.Controllers.Cars
{
    [RequireComponent(typeof(Rigidbody))]
    public class CarController : MonoBehaviour, ISpeedCalculator
    {
        [System.Serializable]
        public class AxleInfo
        {
            public WheelCollider leftWheelCollider;
            public WheelCollider rightWheelCollider;
            public GameObject leftWheelMesh;
            public GameObject rightWheelMesh;
            public bool motor;
            public bool steering;
        }


        [SerializeField] public List<AxleInfo> axleInfos;
        [SerializeField] public float maxMotorTorque;
        [SerializeField] public float maxSteeringAngle;
        [SerializeField] public float brakeTorque;
        [SerializeField] public float decelerationForce;

        private Rigidbody _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void Restart()
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            _rigidbody.velocity = Vector3.zero;
        }

        public float SpeedMetersPerSecond()
        {
            Vector3 wsForward = _rigidbody.transform.rotation * Vector3.forward;
            float vProj = Vector3.Dot(_rigidbody.velocity, wsForward);
            Vector3 projVelocity = vProj * wsForward;
            return projVelocity.magnitude * Mathf.Sign(vProj);
        }

        public void ApplyLocalPositionToVisuals(AxleInfo axleInfo)
        {
            Vector3 position;
            Quaternion rotation;
            axleInfo.leftWheelCollider.GetWorldPose(out position, out rotation);
            axleInfo.leftWheelMesh.transform.position = position;
            axleInfo.leftWheelMesh.transform.rotation = rotation;
            axleInfo.rightWheelCollider.GetWorldPose(out position, out rotation);
            axleInfo.rightWheelMesh.transform.position = position;
            axleInfo.rightWheelMesh.transform.rotation = rotation;
        }

        private void FixedUpdate()
        {
            if (GameController.pauseMode)
            {
                return;
            }

            float motor = maxMotorTorque * Input.GetAxis("Vertical");
            float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

            if (Input.GetKey(KeyCode.R))
            {
                Restart();

                for (int i = 0; i < axleInfos.Count; i++)
                {
                    ApplyLocalPositionToVisuals(axleInfos[i]);
                }
            }

            for (int i = 0; i < axleInfos.Count; i++)
            {
                if (axleInfos[i].steering)
                {
                    Steering(axleInfos[i], steering);
                }
                if (axleInfos[i].motor)
                {
                    Acceleration(axleInfos[i], motor);
                }
                if (Input.GetKey(KeyCode.Space))
                {
                    Brake(axleInfos[i]);
                }
                ApplyLocalPositionToVisuals(axleInfos[i]);
            }
        }

        private void Acceleration(AxleInfo axleInfo, float motor)
        {
            if (motor != 0f)
            {
                axleInfo.leftWheelCollider.brakeTorque = 0;
                axleInfo.rightWheelCollider.brakeTorque = 0;
                axleInfo.leftWheelCollider.motorTorque = motor;
                axleInfo.rightWheelCollider.motorTorque = motor;
            }
            else
            {
                Deceleration(axleInfo);
            }
        }

        private void Deceleration(AxleInfo axleInfo)
        {
            axleInfo.leftWheelCollider.brakeTorque = decelerationForce;
            axleInfo.rightWheelCollider.brakeTorque = decelerationForce;
        }

        private void Steering(AxleInfo axleInfo, float steering)
        {
            axleInfo.leftWheelCollider.steerAngle = steering;
            axleInfo.rightWheelCollider.steerAngle = steering;

            axleInfo.leftWheelCollider.brakeTorque = 0;
            axleInfo.rightWheelCollider.brakeTorque = 0;
        }

        private void Brake(AxleInfo axleInfo)
        {
            axleInfo.leftWheelCollider.brakeTorque = brakeTorque;
            axleInfo.rightWheelCollider.brakeTorque = brakeTorque;
        }
    }
}
